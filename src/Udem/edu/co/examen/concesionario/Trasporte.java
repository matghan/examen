/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Udem.edu.co.examen.concesionario;

/**
 *
 * @author Yeison Andres
 */
public abstract class Trasporte implements Vehiculo{

    
    private String linea;
    private String marca;
    private String color;
    private int capacidadDeCarga;

    public Trasporte(String linea, String marca, String color, int capacidadDeCarga) {
        super();
        this.linea = linea;
        this.marca = marca;
        this.color = color;
        this.capacidadDeCarga = capacidadDeCarga;
    }
//getters and setters
public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCapacidadDeCarga() {
        return capacidadDeCarga;
    }

    public void setCapacidadDeCarga(int capacidadDeCarga) {
        this.capacidadDeCarga = capacidadDeCarga;
    }
    }
